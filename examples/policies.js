module.exports = {
    resource_rules: {
        merge_requests: {
            rules: [{
                name: "No labels",
                conditions: {
                    state: "opened",
                    labels: "none",
                },
                actions: [{
                    name: "label",
                    value: "Statut : To complete",
                },
                {
                    name: "thread",
                    value:  "Hey @{{author.username}}, this merge request do not have any label. To improve comprehension and futur search, please add some.",
                }],
            },{
                name: "Stale MR?",
                conditions: {
                    state: "opened",
                    updated_before: new Date(new Date().setDate(new Date().getDate() - 14)).toLocaleDateString(), // 2 weeks ago
                },
                filters: [{
                    name: "Already marked",
                    filter: function (resource) {
                        return !resource.labels.includes("State: Stale");
                    },
                }],
                actions: [{
                    name: "label",
                    value: "State: Stale",
                },
                {
                    name: "comment",
                    value: "Hey @{{author.username}}, any problem with this MR?",
                }],
            },{
                name: "No remove source branch on merge",
                conditions: {
                    state: "opened",
                },
                filters: [{
                    name: "Remove not configured",
                    filter: function (resource) {
                        return !resource.force_remove_source_branch;
                    },
                }],
                actions: [{
                    name: "update",
                    value: {
                        remove_source_branch: true,
                    },
                }],
            },{
                name: "Too diverged",
                additionnal_infos: true,
                conditions: {
                    state: "opened",
                    wip: "no",
                    include_diverged_commits_count: true,
                },
                filters: [{
                    name: "Already marked",
                    filter: function (resource) {
                        return !resource.labels.includes("Statut : To complete");
                    },
                },{
                    name: "Too diverged",
                    filter: function (resource) {
                        return resource.diverged_commits_count > 50;
                    },
                }],
                actions: [{
                    name: "label",
                    value: "Statut : To complete",
                },{
                    name: "thread",
                    value: `\`{{source_branch}}\` is too far from \`{{target_branch}}\` ({{diverged_commits_count}} commits).
                           Please rebase or merge the target branch to be able to merge.
                           \`\`\`
                           git checkout {{target_branch}}
                           git pull
                           git checkout {{source_branch}}
                           git merge {{target_branch}}
                           \`\`\``,
                }],
            }],
        },
        issues: {
            rules: [],
        },
    },
};
