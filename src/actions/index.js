
const actionsLib = {
    label: require("./label.js"),
    unlabel: require("./unlabel.js"),
    comment: require("./comments.js"),
    thread: require("./thread.js"),
    close: require("./close.js"),
    update: require("./update.js"),
};

module.exports = actionsLib;
