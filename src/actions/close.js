const gitlabAPI = require("../api.js");

module.exports = {
    execute: function (resourceType, resource) {
        return gitlabAPI.post(resourceType + "/" + resource.iid + "/notes", {
            body: "/close",
        });
    },
    dry: function (resourceType, resource) {
        console.debug(`Would close the ${resourceType} ${resource.iid}`);
    },
};
