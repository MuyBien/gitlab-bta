const gitlabAPI = require("../api.js");

module.exports = {
    execute: function (resourceType, resource, value) {
        return gitlabAPI.put(resourceType + "/" + resource.iid, value);
    },
    dry: function (resourceType, resource, value) {
        console.debug(`Would edit the ${value} of ${resourceType} ${resource.iid}`);
    },
};
