const gitlabAPI = require("../api.js");

module.exports = {
    execute: function (resourceType, resource, label) {
        return gitlabAPI.post(resourceType + "/" + resource.iid + "/notes", {
            body: `/label ~"${label}"`,
        });
    },
    dry: function (resourceType, resource, label) {
        console.debug(`Would add the label ${label} to the ${resourceType} ${resource.iid}`);
    },
};
