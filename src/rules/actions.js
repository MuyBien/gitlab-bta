const actionsLib = require("../actions");

module.exports = {
    execute: (resourceType, resource, actions, dry) => {
        actions.forEach(function (action) {
            if (dry) {
                actionsLib[action.name].dry(resourceType, resource, action.value);
            } else {
                console.info(`\t\tApplying action ${action.name} to ${resource.references.short}`);
                actionsLib[action.name].execute(resourceType, resource, action.value).catch(function (error) {
                    console.error(`\x1b[41mError while processing action ${action.name}: ${error.toJSON().message}\x1b[0m`);
                    console.error(error.toJSON());
                    process.exitCode = 1;
                });
            }
        });
    },
};
