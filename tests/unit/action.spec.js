const expect = require("chai").expect;
const nock = require("nock");

const gitlabAPI = require("../../src/api.js");
const actions = require("../../src/actions");

const BASE_URL = "https://gitlab.com";
const PROJECT_ID = 1;

describe("Resources", () => {

    beforeEach(() => {
        gitlabAPI.init({
            "host-url": BASE_URL,
            token: "fakeToken",
            "source-id": PROJECT_ID,
        });

        nock(`${BASE_URL}/api/v4/projects/${PROJECT_ID}`)
            .persist()
            .intercept("/issues/1/notes", "POST")
            .query(true)
            .reply(200, {call: true})
            .intercept("/issues/1/discussions", "POST")
            .query(true)
            .reply(200, {call: true})
            .intercept("/issues/1", "PUT")
            .query(true)
            .reply(200, {call: true});
    });

    describe("Dry-run mode allow to visualize actions", () => {
        it("All actions have a dry-run mode", () => {
            for (let [actionName] of Object.entries(actions)) {
                expect(actions[actionName].dry).to.be.not.undefined;
            }
        });

        it("A dry-run call do not call the API", async () => {
            for (let [actionName] of Object.entries(actions)) {
                const response = await actions[actionName].dry("issues", { id: 1, iid: 1 }, "test");
                expect(response).to.be.undefined;
            }
        });
    });

    describe("Run mode allow to execute actions", () => {
        it("All actions have a execute mode", () => {
            for (let [actionName] of Object.entries(actions)) {
                expect(actions[actionName].execute).to.be.not.undefined;
            }
        });

        it("A run call the API", async () => {
            for (let [actionName] of Object.entries(actions)) {
                const response = await actions[actionName].execute("issues", { id: 1, iid: 1 }, "test");
                expect(response.data.call).to.be.true;
            }
        });
    });
});
