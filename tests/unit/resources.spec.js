const expect = require("chai").expect;
const nock = require("nock");

const gitlabAPI = require("../../src/api.js");
const resources = require("../../src/rules/resources.js");

const BASE_URL = "https://gitlab.com";
const PROJECT_ID = 1;

describe("Resources", () => {

    beforeEach(() => {
        gitlabAPI.init({
            "host-url": BASE_URL,
            token: "fakeToken",
            "source-id": PROJECT_ID,
        });

        nock(`${BASE_URL}/api/v4/projects/${PROJECT_ID}`)
            .get("/issues?state=opened")
            .reply(200, [{
                id: 1,
                iid: 1,
            },{
                id: 2,
                iid: 2,
            }]);
        nock(`${BASE_URL}/api/v4/projects/${PROJECT_ID}`)
            .get("/issues/1?state=opened")
            .reply(200, {
                id: 1,
                iid: 1,
                more: true,
            });
        nock(`${BASE_URL}/api/v4/projects/${PROJECT_ID}`)
            .get("/issues/2?state=opened")
            .reply(200, {
                id: 2,
                iid: 2,
                more: true,
            });
    });

    describe("List resources", () => {
        it("Call the good API endpoint to get resources", () => {
            return resources.get("issues", { state: "opened" }).then(response => {
                expect(response.length).to.equal(2);
            });
        });

        it("Call the detail endpoint for each resource if additional_info is specified", () => {
            return resources.getComplete("issues", { state: "opened" }).then(response => {
                expect(response.length).to.equal(2);
                expect(response[0].more).to.be.true;
            });
        });

    });

});
