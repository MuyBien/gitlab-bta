const expect = require("chai").expect;
const filterLib = require("../../src/rules/filters.js");

describe("Filters", () => {

    const issues = [{
        id: 1,
        iid: 1,
        labels: ["State: Stale", "State: Test"],
    },{
        id: 2,
        iid: 2,
        labels: ["State: Test"],
    }];

    it("Apply a filter function to a list of resources", () => {
        const filters = [{
            name: "Already marked",
            filter: function (resource) {
                return !resource.labels.includes("State: Stale");
            },
        }];
        const filteredIssues = filterLib.apply(issues, filters);
        expect(filteredIssues.length).to.be.equals(1);
    });
});
